#!/bin/bash
#general requirements
pip install -r requirements1.txt
pip install torch==1.9.1+cu102 torchvision==0.10.1+cu102 torchaudio===0.9.1 -f https://download.pytorch.org/whl/torch_stable.html
pip install -r requirements2.txt
git clone https://github.com/microsoft/DeepSpeed -b v0.5.3
cd DeepSpeed
# https://pypi.org/project/deepspeed/0.5.3/
DS_BUILD_OPS=1 pip install .
ds_report
pip freeze
PAUSE
cmd /k