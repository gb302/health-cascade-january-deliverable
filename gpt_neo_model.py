import torch
from transformers import GPT2Tokenizer, GPTNeoForCausalLM, TrainingArguments, Trainer, pipeline, GPT2LMHeadModel, AutoModelForCausalLM,BertTokenizer
import pandas as pd
from datasets import Dataset
import os, re, gc
#device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
#device = torch.device("cpu")
def gpt_tokenizer(token_name):    
    # Download the pre-trained GPT-Neo model's tokenizer
    # Add the custom tokens denoting the beginning and the end 
    # of the sequence and a special token for padding
    tokenizer = GPT2Tokenizer.from_pretrained(token_name,    
                                bos_token='<|startoftext|>',
                                eos_token='<|endoftext|>',
                                pad_token='<|pad|>')
    return tokenizer

def gpt_model(model_name,tokenizer):
    model = GPTNeoForCausalLM.from_pretrained(model_name)
    # model = AutoModelForCausalLM.from_pretrained(model_name).to(device = device)

    # Resize the token embeddings because we've just added 3 new tokens 
    model.resize_token_embeddings(len(tokenizer))
    return model


def read_text_file(file_name, tokenizer,range_start = None, range_end = None):
    #descriptions = pd.read_csv(file_name,sep='\n\n',header = None, engine = 'python')
    with open(file_name,'r') as f:
        descriptions = f.read()
    #descriptions = descriptions.split('.\n | \n\n | ;\n')
    descriptions = re.split('.\n|\n\n|;\n', descriptions)
    # for i in range(len(descriptions)):
    #     descriptions[i] = re.sub("\n", " " , descriptions[i])
    if range_start or range_end:
        print(range_start, range_end)
        descriptions = descriptions[range_start:range_end]
    max_length = max([len(tokenizer.encode(description)) for description in descriptions])
    return descriptions, max_length


class create_dataset():
  def __init__(self, txt_list, tokenizer, max_length):
      
        self.input_ids = []
        self.attn_masks = []
        self.labels = []
        for txt in txt_list:
            # Encode the descriptions using the GPT-Neo tokenizer
            encodings_dict = tokenizer('<|endoftext|>' 
                                        + txt +    
                                        '<|endoftext|>',
                                        truncation=True,
                                        max_length=max_length, 
                                          padding="max_length")
            input_ids = torch.tensor(encodings_dict['input_ids'])
            self.input_ids.append(input_ids)
            mask = torch.tensor(encodings_dict['attention_mask'])
            self.attn_masks.append(mask)
  def __len__(self):
    return len(self.input_ids)
  def __getitem__(self, idx):
    return self.input_ids[idx], self.attn_masks[idx]

def split_data(data,num_train = 0.9):        
    train_size = int(num_train * len(data))
    train_dataset, val_dataset = torch.utils.data.random_split(data,
    [train_size, len(data) - train_size])
    return train_dataset, val_dataset


def fine_tune_model(model, tokenizer, batch_size, train_data, val_data, dev):
    
    training_args = TrainingArguments(output_dir='./results',
                                    num_train_epochs=5,
                                    logging_steps=5000,
                                    save_steps=5000,                           
                                    per_device_train_batch_size=batch_size,
                                    per_device_eval_batch_size=batch_size,
                                    warmup_steps=100,
                                    weight_decay=0.01,  
                                    logging_dir='./logs',
                                    no_cuda = dev)
                                    
    print(device)
    model = model.to(device = device)
    print(model.device)

    trainer = Trainer(model=model, args=training_args,  
                    train_dataset=train_data,
                    eval_dataset=val_data, 
                    # This custom collate function is necessary 
                    # to built batches of data
                    data_collator=lambda data: 
                {'input_ids': torch.stack([f[0] for f in data]),       
                'attention_mask': torch.stack([f[1] for f in data]),
                'labels': torch.stack([f[0] for f in data])})
    # Start training process!
    print("asdfasdfasdf",training_args.device)
    trainer.train()
    return model, training_args

def generate_text(prompt,model,tokenizer,args = None ,save_text = None):
# Start every description with a special BOS token
    # print(tokenizer)
    # print(type(tokenizer))
    tokenizer = GPT2Tokenizer.from_pretrained(tokenizer)
    print(tokenizer)
    model = GPTNeoForCausalLM.from_pretrained(model)
    generated = tokenizer(prompt,add_special_tokens=False, return_tensors="pt").input_ids.to(device = device)
    print(generated)
    model = model.to(device = device)
    # print("{}{}".format("prompt lenght is : ",generated.size()[1]))
    # Generate 3 movie descriptions
    # print(args)
    #os.command("nvidia-smi")
    # if args  is None :
    #     text_generator = pipeline("text-generation", model=model, tokenizer = tokenizer)
    #     text = text_generator(prompt, max_length=300, do_sample=True, 
    #                     # Keep only top 50 token with 
    #                     # the highest probability
    #                     top_k=50, 
    #                     # Maximum sequence length
    #                     # Keep only the most probable tokens 
    #                     # with cumulative probability of 95%
    #                     top_p=0.95, 
    #                     # Changes randomness of generated sequences
    #                     temperature=0.7,
    #                     # Number of sequences to generate   
    #                     no_repeat_ngram_size = 2,
    #                     num_beams=5,    
    #                     num_return_sequences=3)
    # else:
    #     text_generator = pipeline("text-generation", model=model, tokenizer = tokenizer)
    #     text = text_generator(prompt, max_length=args['max_length'], do_sample=True, 
    #                     # Keep only top 50 token with 
    #                     # the highest probability
    #                     top_k=args['top_k'], 
    #                     # Maximum sequence length
    #                     # Keep only the most probable tokens 
    #                     # with cumulative probability of 95%
    #                     top_p=args['top_p'], 
    #                     # Changes randomness of generated sequences
    #                     temperature=args['temperature'],
    #                     no_repeat_ngram_size = 3,
    #                     # #repetition_penalty  = 1.5,
    #                     # num_beams=5,
    #                     # early_stopping = True,
    #                     # Number of sequences to generate                 
    #                     num_return_sequences=args['num_return_sequences'])
    
    # sample_outputs = []
    # for i in range(len(text)):
    #     sample_outputs.append(text[i]['generated_text'])
    #     print("#######################################################")
    #     print(len(text[i]['generated_text'].split(" ")))        
    #     print("#######################################################")
    #     print(text[i]['generated_text'])

    #print(sample_outputs)
    

    text = model.generate(generated)
                    # # Use sampling instead of greedy decoding 
                    # do_sample=True, max_length=args['max_length']+generated.size()[1],
                    #     # Keep only top 50 token with 
                    #     # the highest probability
                    #     top_k=args['top_k'], 
                    #     # Maximum sequence length
                    #     # Keep only the most probable tokens 
                    #     # with cumulative probability of 95%
                    #     top_p=args['top_p'], 
                    #     # Changes randomness of generated sequences
                    #     temperature=args['temperature'],
                    #     #no_repeat_ngram_size = 2,
                    #     # repetition_penalty  = 1.1,
                    #     # num_beams=5,
                    #     # early_stopping = True,
                    #     # Number of sequences to generate                 
                    #     num_return_sequences=args['num_return_sequences'])
                        
    # text = model.generate(generated,  max_length=300, do_sample=True, 
    #                     # Keep only top 50 token with 
    #                     # the highest probability
    #                     top_k=50, 
    #                     # Maximum sequence length
    #                     # Keep only the most probable tokens 
    #                     # with cumulative probability of 95%
    #                     top_p=0.95, 
    #                     # Changes randomness of generated sequences
    #                     temperature=0.7,
    #                     # Number of sequences to generate   
    #                     no_repeat_ngram_size = 2,
    #                     num_return_sequences=3)

    # sample_outputs = []
    # for i in range(len(text)):
    #     sample_outputs.append(text[i]['generated_text'])
    #     print("#######################################################")
    #     print(len(text[i]['generated_text'].split(" ")))        
    #     print("#######################################################")
    #     print(text[i]['generated_text'])
    # os.command("nvidia-smi")
    # Print generated descriptions
    text_output = []
    for i, sample_output in enumerate(text): 
        text_output.append("{} \n\n".format(str(tokenizer.decode(sample_output, 
                                    skip_special_tokens=True))))
    print(text_output)
    if save_text:
        save_output(sample_outputs ,save_text, tokenizer)   

    del generated
    del text
    gc.collect()
    torch.cuda.empty_cache() 

    return text_output



def save_tokenizer(file, tokenizer):
    tokenizer.save_pretrained(file)
    return tokenizer

def save_tuned_model(file, model):
    model.save_pretrained(file)
    return model

def save_output(output,save_file_name,tokenizer):
    with open(save_file_name, "w") as f:
        # #f.write("Model: {} \n".format(model_name))
        # f.write("length of fine tune inputs: {} \n".format(max_length))
        # # f.write("Model training params: {} \n".format(training_args))
        # # f.write("Model generation params: {} \n".format(training_args))
        for i, sample_output in enumerate(output): 
            f.write("{}: {} \n".format(i, str(tokenizer.decode(sample_output, 
                                    skip_special_tokens=True,)).encode("utf-8")))
            # print("{}: {}".format(i, tokenizer.decode(sample_output, 
            #                             skip_special_tokens=True,clean_up_tokenization_spaces=False,)).encode("utf-8"))
        f.close()
    return save_file_name

def train_model(inputs, model_name, token_name,dev, state = None, range_start = None, range_end = None):
    print("start training")
    tokenizer = gpt_tokenizer(token_name)
    print("loaded tokenizer")
    model = gpt_model(model_name,tokenizer)
    print("loaded model")        
    descriptions, max_length = read_text_file(inputs,tokenizer,range_start = range_start, range_end = range_end)
    print(len(descriptions))
    print(max_length)
    print("loaded text")
    dataset = create_dataset(descriptions, tokenizer, max_length = max_length)
    print("Added data to tokenizer")
    train_dataset, val_dataset = split_data(dataset,0.9)
    print(len(train_dataset),len(val_dataset))
    print("split Data")
    
    trained_model,training_args = fine_tune_model(model,tokenizer,1,train_dataset,val_dataset,dev)
    print("fine-tuned model")
    if state:
        save_tokenizer(state,tokenizer)
        save_tuned_model(state,trained_model)
    return trained_model, tokenizer, max_length, training_args

def train_and_generate(prompt, inputs, model_name, token_name, dev, state = None, save_text = None, range_start = None, range_end = None):
  
    print(device)
    model, tokenizer, max_length, training_args = train_model(inputs, model_name,token_name, dev, state, range_start, range_end)
    output_text = generate_text(prompt, model, tokenizer, None, save_text)
    print("finished")
    return output_text

def generate_from_pretrained(prompt, model_name, token_name, args, save_text = None):
    print("generate_from " , model_name)

    print("start generation")
    #tokenizer = gpt_tokenizer(token_name)
    print("loaded tokenizer")
    #model = gpt_model(model_name,tokenizer)
    #print("##################### MODEL_NAME/MODEL_NAME###########################")
    text1 = generate_text(prompt, model_name, token_name, args, save_text)

    print("loaded model")     
    print("finished")

    # from transformers import pipeline
    # generator = pipeline('text-generation', model=model_name)
    # print("Model has been generated...")
    # text = generator(prompt, do_sample=False, max_length=300,temperature = 0.7,no_repeat_ngram_size =3)    
    # print("Text has been generated...")
    # print(text)

    return text1

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# torch.manual_seed(42)# Set the random seed to a fixed value to get reproducible results 

# token_name = "gpt2"

# dev = False
# token_name = "allenai/scibert_scivocab_uncased"

# #model_name = "EleutherAI/gpt-neo-1.3B"
# model_name = "allenai/scibert_scivocab_uncased"
# if dev is False:
#     device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# else:
#     device = torch.device("cpu")
# inputs = ".\shakespeare.txt"
# # #device_name = "cuda"
# # #device = torch.device(device_name)
# prompt = "What is the speed of light?" 
# output = "./results/output.txt"
# state = "./state/"
# #asdf = train_and_generate(prompt,inputs,model_name,token_name, dev,state, range_end = 2000)
# print("#########################################################################")
# asdf = generate_from_pretrained(prompt,model_name,token_name,None)
# print(type(asdf))
# print(asdf)
