from flask import Flask,render_template, request
from flask_restx import Api, Resource, fields ,reqparse, marshal_with, marshal
from datetime import datetime
import logging
import copy
from gpt_neo_model import *
import json


logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
api = Api(app)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
# db = SQLAlchemy(app)

name_space = api.namespace(
    'model_training',
    description='This is a sevice for training and deploying models.')

###train(prompt, inputs, model_name, state = None, save_text = None, range_start = None, range_end = None):
a_train = api.model(
    'Train GPT-Neo model', {
        'prompt': fields.String(description="Path of the input file",default = 'GPT is ',required = True),
        'inputs': fields.String(description="Path of the input file",default = '.\input.txt',required = True),
        'model_name': fields.String(description="Model to use", default = 'EleutherAI/gpt-neo-2.7B',required = True),
        'state': fields.Raw(description="Provide path to save the model, leave empty to not.",required = False),
        'save_text': fields.Raw(description="Provide path to save the model, leave empty to not.",required = False),
        'range_start': fields.Raw(description="At which paragraph the training should start",default = None,required = False),
        'range_end': fields.Raw(description="At which paragragh the training should end.", default = None, required = False)})
#generate(prompt,model,save_text)(prompt, model_name,save_text = None)

a_generate = api.model(
    'Generate GPT-Neo model', {
        'prompt': fields.String(description="Path of the input file",default = 'GPT is ',required = True),
        'model_name': fields.String(description="Model to use", default = 'EleutherAI/gpt-neo-2.7B',required = True),
        'save_text': fields.Raw(description="At which paragragh the training should end.", default = None, required = False)})


generate_model_args = reqparse.RequestParser()
generate_model_args.add_argument('prompt', type=str, help="Name of video is required",required = True)
generate_model_args.add_argument('model_name', type=str, help="views of video is required",required = True)
generate_model_args.add_argument('max_length', type=int, help="likes of video is required",required = False)
generate_model_args.add_argument('top_k', type=int, help="likes of video is required",required = False)
generate_model_args.add_argument('top_p', type=float, help="likes of video is required",required = False)
generate_model_args.add_argument('temperature', type=float, help="likes of video is required",required = False)
generate_model_args.add_argument('num_return_sequences', type=int, help="likes of video is required",required = False)
generate_model_args.add_argument('password', type=str, help="likes of video is required",required = False)

ns1 = api.namespace('api/v1', description='test')
fh = logging.FileHandler("v1.log")
ns1.logger.addHandler(fh)

resource_field = {
   'prompt' : fields.String,
   'model_name' : fields.String,
   'max_length' : fields.Integer,
   'top_k' : fields.Integer,
   'top_p' : fields.Float,
   'temperature' : fields.Float,
   'num_return_sequences' : fields.Integer,
   'password' : fields.String
}
@app.route("/home")
def ssss():
    return render_template("index.html")

@app.route("/upload")
def sss1s():
    return render_template("results.html")
	
@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['file']
      f.save(f.filename)
      return 'file uploaded successfully'
		
@name_space.route('/train_generate')
class api_train_model(Resource):

    @api.expect(a_train)
    def post(self):
        
        
        ns1.logger.info('- Forecast Request')
        ns1.logger.info(datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
        ns1.logger.info(api.payload)
        print(api.payload)
        prompt= api.payload['prompt']
        inputs = api.payload['inputs']
        model_name = api.payload['model_name']
        state = api.payload['state']
        save_text = api.payload['save_text']
        range_start = api.payload['range_start']
        range_end = api.payload['range_end']
        print(type(range_end))
        print(range_end)
        print(range_start)
        output_text = train_and_generate(prompt,inputs,model_name,state,save_text,range_start,range_end)
        print("we here")
        print(type(output_text))
        #output_text = {'output': output_text}
        return output_text

@name_space.route('/generate')
class api_generate_model(Resource):

    #@api.expect(a_generate)
    #@api.marshal_with(resource_field)
    @api.expect(a_generate)
    def get(self):
        
        args = generate_model_args.parse_args()
          
        password = args['password']
        print(password)
        print(type(password))
        if password == '1234':
                
            #prompt= api.payload['prompt']#"Lord Marshal: "#api.payload['prompt']
            prompt = args['prompt']
            model_name = args['model_name']#api.payload['model_name']
            max_length = args['max_length']
            top_k = args['top_k']
            top_p = args['top_p']
            temperature = args['temperature']
            num_return_sequences = args['num_return_sequences']      
            gen_args = {"max_length" : args['max_length'], "top_k" : args['top_k'],
                        "top_p" : args['top_p'], "temperature" : args['temperature'],
                        "num_return_sequences" : args['num_return_sequences']}
            print("@api   ",gen_args,top_k)
            print("@api   ",model_name)
            save_text = None#api.payload['save_text']
            output_text = generate_from_pretrained(prompt,model_name,model_name,gen_args,save_text)
            print("we here")
            print(type(output_text))
            output_dict = {'output': output_text}
            print("#########output_dict['output']########")
            print(output_dict['output'])
            # #output_json = json.dumps(output_text)
            # with open("asdf.json", "w") as f:
            #     json.dump(output_dict,f)

            # print(type(output_text),output_text)
            # print("============")
            # print(type(output_dict),output_dict)
            # print("============")
            # print(type(output_json),output_json)
            # print("============")
        
            return output_text, 201
        else:
            return "wrong password", 202




###generate




###train_generate

if __name__ == "__main__":
    app.run(host="127.0.0.1",port=8080,debug=True,use_reloader=False)